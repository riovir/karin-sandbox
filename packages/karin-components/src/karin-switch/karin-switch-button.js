import { unsafeCSS } from '@lion/core';
import { LionSwitchButton } from '@lion/switch';
import styles from './karin-switch-button.scss';

export class KarinSwitchButton extends LionSwitchButton {
	static get styles() {
		return [super.styles, unsafeCSS(styles)];
	}
}
