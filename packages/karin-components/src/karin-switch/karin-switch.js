import { unsafeCSS } from '@lion/core';
import { LionSwitch } from '@lion/switch';
import { KarinSwitchButton } from './karin-switch-button';
import styles from './karin-switch.scss';

export class KarinSwitch extends LionSwitch {
	static get scopedElements() {
		return {
			...super.scopedElements,
			'lion-switch-button': KarinSwitchButton,
		};
	}

	static get styles() {
		return [super.styles, unsafeCSS(styles)];
	}
}
