import { KarinSwitch } from '@riovir/karin-components-vue';

export default {
	title: 'components / karin-switch',
	component: KarinSwitch,
};

const Template = args => ({
	template: `
		<div style="display: grid; grid-gap: 1rem;">
			<karin-switch v-bind="{ disabled, label }" :checked="true" />
			<karin-switch v-bind="{ disabled, label }" :checked="false" />
		</div>
	`,
	components: { KarinSwitch },
	setup: () => args,
});

export const base = Template.bind({});
base.args = { label: 'Label', disabled: false };

export const disabled = Template.bind({});
disabled.args = { ...base.args, disabled: true };

export const sizes = args => ({
	template: `
		<div style="display: grid; grid-gap: 1rem;">
			<karin-switch v-bind="{ checked, label }" class="is-small" />
			<karin-switch v-bind="{ checked, label }" />
			<karin-switch v-bind="{ checked, label }" class="is-medium" />
			<karin-switch v-bind="{ checked, label }" class="is-large" />
		</div>
	`,
	components: { KarinSwitch },
	setup: () => args,
});
sizes.args = base.args;
