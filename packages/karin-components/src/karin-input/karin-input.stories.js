import { KarinInput } from '@riovir/karin-components-vue';

export default {
	title: 'components / karin-input',
	component: KarinInput,
};


export const base = args => ({
	template: `
		<karin-input>
			<template #label><label>{{ label }}</label></template>
			<template #help-text><div>{{ helpText }}</div></template>
		</karin-input>
	`,
	components: { KarinInput },
	setup: () => args,
});
base.args = { label: '', helpText: 'Fill in your name' };

export const sizes = args => ({
	template: `
		<div style="display: grid; grid-gap: 1rem;">
			<karin-input class="is-small">
				<template #label><label>{{ label }}</label></template>
				<template #help-text><div>{{ helpText }}</div></template>
			</karin-input>
			<karin-input>
				<template #label><label>{{ label }}</label></template>
				<template #help-text><div>{{ helpText }}</div></template>
			</karin-input>
			<karin-input class="is-medium">
				<template #label><label>{{ label }}</label></template>
				<template #help-text><div>{{ helpText }}</div></template>
			</karin-input>
			<karin-input class="is-large">
				<template #label><label>{{ label }}</label></template>
				<template #help-text><div>{{ helpText }}</div></template>
			</karin-input>
		</div>
	`,
	components: { KarinInput },
	setup: () => args,
});
sizes.args = base.args;
