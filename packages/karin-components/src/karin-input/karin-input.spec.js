import { fixture, expect } from '@open-wc/testing';
import { KarinInput } from './karin-input.js';

customElements.define('karin-input', KarinInput);

describe('Plugin - chai-dom', () => {
	it('can check for an exiting css class', async () => {
		const el = await fixture('<karin-input class="foo bar"></karin-input>');
		expect(el).to.have.class('foo');
	});
});
