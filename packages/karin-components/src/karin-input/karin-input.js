import { unsafeCSS } from '@lion/core';
import { LionInput } from '@lion/input';
import styles from './karin-input.scss';

export class KarinInput extends LionInput {
	static get styles() {
		return [super.styles, unsafeCSS(styles)];
	}
}
