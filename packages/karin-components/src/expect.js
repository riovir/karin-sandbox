export function expect(result) {
	const toBe = expected => {
		if (expected !== result) {
			throw new Error(`Expected: ${expected}. Got: ${result}.`);
		}
	};
	return { toBe };
}
