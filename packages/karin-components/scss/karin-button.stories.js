
export default { title: 'scss / karin-button' };

export const base = () => ({ template: `
	<div style="display: flex; gap: 1rem; ">
		<button class="button is-primary">Primary button</button>
		<button class="button is-primary is-hovered">Primary button</button>
		<button class="button is-primary is-active">Primary button</button>
		<button class="button is-primary" disabled>Primary button</button>
	</div>
` });
