export default { title: 'scss / karin-input' };

const Template = ({ placeholder }) => ({ template: `
	<label for="input-234234">Name</label>
	<input id="input-234234" class="input" placeholder="${placeholder}">
` });

export const base = Template.bind({});
base.args = { placeholder: 'Enter your name...' };

export const sizes = ({ placeholder }) =>  ({ template: `
	<div style="display: grid; grid-gap: 1rem;">
		<input class="input is-small" placeholder="${placeholder}">
		<input class="input" placeholder="${placeholder}">
		<input class="input is-medium" placeholder="${placeholder}">
		<input class="input is-large" placeholder="${placeholder}">
	</div>
` });
sizes.args = base.args;
