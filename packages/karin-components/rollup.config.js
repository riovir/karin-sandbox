import path from 'path';
import { terser } from 'rollup-plugin-terser';
import visualizer from 'rollup-plugin-visualizer';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import postCss from 'rollup-plugin-postcss';
import pkg from './package.json';

export default [
	Bundle({
		file: pkg.module,
		format: 'es',
		external: [/@lion\//],
		plugins: [visualizer()],
	}),
	Bundle({
		file: pkg.main,
		format: 'cjs',
		plugins: [nodeResolve()],
		context: 'globalThis',
	}),
];

function Bundle({ file, format, plugins = [], ...rest }) {
	return {
		input: 'src/index.js',
		output: { file, format },
		plugins: [
			...plugins,
			postCss({ use: [
				['sass', { includePaths: [
					path.resolve('node_modules'),
					path.resolve('../../node_modules'),
				] }],
			]	}),
			terser(),
		],
		...rest,
	};
}
