module.exports = {
	presets: [
		['@babel/preset-env', {
			modules: false,
			corejs: 3,
			useBuiltIns: 'usage',
		}],
	],
	plugins: ['@babel/plugin-proposal-optional-chaining'],
	env: {
		test: {
			presets: [
				['@babel/preset-env', { targets: { node: 'current' } }],
			],
		},
	},
};
