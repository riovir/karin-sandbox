import { KarinSwitchButton } from './index';

export default {
	title: 'components / karin-switch-button',
	component: KarinSwitchButton,
};

const Template = args => ({
	template: `
		<karin-switch-button v-bind="{ disabled }" :checked="true" />
		<karin-switch-button v-bind="{ disabled }" :checked="false" />
	`,
	components: { KarinSwitchButton },
	setup: () => args
});

export const base = Template.bind({});
base.args = { disabled: false };

export const disabled = Template.bind({});
disabled.args = { ...base.args, disabled: true };

export const sizes = args => ({
	template: `
		<div style="display: grid; grid-gap: 1rem;">
			<karin-switch-button v-bind="{ checked }" class="is-small"></karin-switch-button>
			<karin-switch-button v-bind="{ checked }"></karin-switch-button>
			<karin-switch-button v-bind="{ checked }" class="is-medium"></karin-switch-button>
			<karin-switch-button v-bind="{ checked }" class="is-large"></karin-switch-button>
		</div>
	`,
	components: { KarinSwitchButton },
	setup: () => args
})
sizes.args = { checked: false };
