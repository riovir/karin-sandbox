import {
	KarinInput as Input,
	KarinSwitch as Switch,
	KarinSwitchButton as SwitchButton,
} from '@riovir/karin-components';
import { toVueComponent } from './webcomponents';

export const KarinInput = toVueComponent(Input, { name: 'KarinInput' });
export const KarinSwitch = toVueComponent(Switch, { name: 'KarinSwitch' });
export const KarinSwitchButton = toVueComponent(SwitchButton, { name: 'KarinSwitchButton' });

