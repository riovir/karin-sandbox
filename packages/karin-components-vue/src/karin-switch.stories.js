import { KarinSwitch } from './index';

export default {
	title: 'components / karin-switch',
	component: KarinSwitch,
};

const Template = (args) => ({
	template: `
		<div style="display: grid; grid-gap: 1rem;">
			<karin-switch class="is-small" v-bind="args" />
			<karin-switch v-bind="args" />
			<karin-switch class="is-medium" v-bind="args" />
			<karin-switch class="is-large" v-bind="args" />
		</div>
	`,
	components: { KarinSwitch },
	setup: () => ({ args })
});

export const base = Template.bind({});
base.args = { checked: false, label: 'Label' };
