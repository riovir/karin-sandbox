import { terser } from 'rollup-plugin-terser';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import pkg from './package.json';

export default [
	Bundle({
		file: pkg.module,
		format: 'es',
		external: [/@riovir\//, 'vue'],
	}),
	Bundle({
		file: pkg.main,
		format: 'cjs',
		plugins: [nodeResolve()],
		context: 'globalThis',
	}),
];

function Bundle({ file, format, plugins = [], ...rest }) {
	return {
		input: 'src/index.js',
		output: { file, format },
		plugins: [
			...plugins,
			terser(),
		],
		...rest,
	};
}
