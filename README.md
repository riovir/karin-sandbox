# @riovir/karin-sandbox [![Storybook](https://cdn.jsdelivr.net/gh/storybookjs/brand@master/badge/badge-storybook.svg)](https://riovir.gitlab.io/karin-sandbox/)

> Prototype project to explore [Bulma](https://bulma.io/) + [Lion Components](https://lion-web-components.netlify.app/) interoperability.

**For development, don't forget to read the [CONTRIBUTING.md](./CONTRIBUTING.md).**

## Convenience: VS Code snippets

If you are using Visual Studio Code, 
press `Ctrl + Shift + P` > type `snip` > select `Preferences: Configure user snippets` > select `javascript.json`

Add these entries to the end of the json file that opens:

```json
"Webcomponent - vanilla": {
    "prefix": "wcvanilla",
    "body": [
        "const template = document.createElement('template');",
        "template.innerHTML = /* html */`",
        "<style>",
        "\t:host {",
        "\t\tbox-sizing: border-box;",
        "\t\tdisplay: block;",
        "\t}",
        "\t.hello {",
        "\t\tborder-radius: 0.25em;",
        "\t\tpadding: 0.25em 0.5em;",
        "\t\tbox-shadow: inset 0px 0.125em 0.5em rgba(51, 51, 51, 0.3);",
        "\t}",
        "</style>",
        "<div id=\"hello\">Hello world</div>",
        "`;\n",
        "class ${1:MyComponent} extends HTMLElement {",
        "\tconstructor() {",
        "\t\tsuper();",
        "\t\tconst shadowRoot = this.attachShadow({ mode: 'open' });",
        "\t\tshadowRoot.appendChild(template.content.cloneNode(true));",
        "\t\tthis._hello = 'Hello world';",
        "\t\tthis._helloElement = shadowRoot.querySelector('#hello');",
        "\t}\n",
        "\tconnectedCallback() {",
        "\t\tthis.hello = this._hello;",
        "\t}\n",
        "\tset hello(value) {",
        "\t\tthis._hello = value;",
        "\t\tthis._helloElement.textContent = this._hello;",
        "\t}\n",
        "\tget hello() {",
        "\t\treturn this._hello;",
        "\t}",
        "}\n",
        "customElements.define('${3:my-component}', $1);"
    ],
    "description": "A webcomponent - Vanilla"
},

"Storybook - Webcomponent - Meta": {
    "prefix": "stwmeta",
    "body": [
        "import { html } from 'lit-html';\n",
        "export default { title: '${3:components} / ${2: my-component}' };\n$0",
    ],
    "description": "Title metadata for a Webcomponent Storybook stories file"
},
"Storybook - Webcomponent - Template": {
    "prefix": "stwtemplate",
    "body": [
        "const Template = ({ $4 }) => html`",
        "\t<${1:my-component}$2>$3</$1>",
        "`;\n$0"
    ],
    "description": "Template function for arg driven Webcomponent stories, best used for app development"
},
"Storybook - Webcomponent - Story": {
    "prefix": "stwstory",
    "body": [
        "export const ${1:base} = () => html`",
        "\t<${2:my-component}$3>$4</$2>",
        "`;\n$0"
    ],
    "description": "Inline story format for Webcomponent, best used for documentation purposes"
},

"Storybook - Arg Story": {
    "prefix": "stoarg",
    "body": [
        "export const ${1:base} = Template.bind({});",
        "$1.args = { $2 };$0"
    ],
    "description": "Arg driven base story exporting a copy of the Template function with args set on it"
},
"Storybook - Arg Derived Story": {
    "prefix": "stodarg",
    "body": [
        "export const ${1:varation} = Template.bind({});",
        "$1.args = { ...${2:base}.args, $3 };$0"
    ],
    "description": "Derived arg driven story pulling most values from a base story"
}
```

**Now in js files you can type:**
- **wcv**anilla to drop down a no-depenency hello-world webcomponent to your cursor.
- **stwm**eta to drop a Storybook Meta entry (1x per .stories.js)
- **stws**tory to drop a one-off Story (as many as you need)
- **stwt**emplate to create reusable Story Template (1x per .stories.js typically)
- **stoa**rg to create the base Story based on the Template (1x per .stories.js)
- **stod**arg to create a derived Story that differs in some way from the base one (as many as you need)
