const path = require('path');

module.exports = async ({ config }) => {
	config.resolve.alias = {
		...config.resolve.alias,
		'@': __dirname,
		// '@riovir/karin-components/fonts': path.resolve(__dirname, '../fonts'),
		// '@riovir/karin-components/scss': path.resolve(__dirname, '../scss'),
		// '@riovir/karin-components': path.resolve(__dirname, '../dist'),
		// '@riovir/karin-components-vue': path.resolve(__dirname, '../../karin-components-vue/src'),
	};
	config.resolve.fallback = {
		...config.resolve.fallback,
		crypto: require.resolve('crypto-browserify'),
		stream: require.resolve('stream-browserify'),
	};

	const { use: cssLoaders } = findRuleMatching('any.css', config.module.rules);
	config.module.rules = [
		...config.module.rules,
		{ test: /\.(scss|sass)$/, use: [...cssLoaders, 'resolve-url-loader', 'sass-loader'] },
	];
	return config;
};

function findRuleMatching(name, rules) {
	return rules.find(({ test }) => test.test(name));
}
