import Vue from 'vue/dist/vue.esm';

export function withVue(Story, { args = {} }) {
	const div = document.createElement('div');
	const VueStory = Vue.extend({
		template: `
			<the-story v-bind="args" />
		`,
		data: () => ({ args }),
		components: { TheStory: Story() },
	});
	const storyComponent = new VueStory().$mount();
	div.appendChild(storyComponent.$el);
	return div;
}
