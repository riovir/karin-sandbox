module.exports = {
	stories: [
		'./**/*.stories.@(js|mdx)',
		'../packages/**/*.stories.@(js|mdx)',
	],
	addons: [
		'@storybook/addon-essentials',
		'@storybook/addon-a11y',
	],
	core: { builder: 'webpack5' },
};
