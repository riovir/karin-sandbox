import { puppeteerLauncher } from '@web/test-runner-puppeteer';

export default {
  browsers: [puppeteerLauncher({ launchOptions: { args: ['--no-sandbox'] } })],
	middleware: [Sass({
		toCssModule: sassUrl => `export default '/* CSS mock of ${sassUrl} */';`
	})],
};

function Sass({ toCssModule }) {
	return async function sass(ctx, next) {
		if (!/\.(scss|scss)$/.test(ctx.originalUrl)) {
			return next();
		}
		ctx.set('cache-control', 'no-cache');
		ctx.set('content-type', 'application/javascript; charset=utf-8');
		ctx.response.body = await toCssModule(ctx.originalUrl);
	};
}
